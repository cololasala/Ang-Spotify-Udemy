import { Component } from '@angular/core';
import { SearchService } from '../services/search.service';
import { TrackModel } from '@core/models/track.model';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent {
  dataTracks: Observable<TrackModel[]> = of([]); // inicializamos obsevable con array vacio
  constructor(private searchService: SearchService) {}

  searched(e: string): void {
    this.dataTracks = this.searchService.getTracks(e);
  }
}
